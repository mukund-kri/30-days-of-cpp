set boxwidth 0.5
set style fill solid
set xtics rotate by 45 right
set xtics center offset 0,-1.1
plot "problem3.dat" using 1:3:xtic(2) with boxes
