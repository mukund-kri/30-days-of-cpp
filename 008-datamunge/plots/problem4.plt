red = "#FF0000"; green = "#00FF00"; blue = "#0000FF"; skyblue = "#87CEEB"; megenta = "#FF00FF";

set style data histogram
set yrange [0:6000]
set style histogram cluster gap 1
set style fill solid
set boxwidth 0.9
set xtics format ""
set grid ytics

set title "A Sample Bar Chart"
plot "problem4.dat" using 2:xtic(1) title "Business Services" linecolor rgb red, \
            '' using 3 title "Real Estate and Renting" linecolor rgb blue, \
            '' using 4 title "Trading" linecolor rgb green, \
            '' using 5 title "Construction" linecolor rgb skyblue, \
	    '' using 6 title "Community, Personal" linecolor rgb megenta
