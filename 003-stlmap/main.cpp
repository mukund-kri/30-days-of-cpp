/*
 * Explore how to do maps in c++
 */
#include <iostream>
#include <string>
#include <map>


void addKeyValue(std::map<std::string, int>* dict, std::string key, int value) {
  dict->insert(std::pair<std::string, int>(key, value));
}

int main() {

  // declare a map
  std::map<std::string, int> nameNums;

  // Add elements to the map
  nameNums["One"] = 1;

  // Access that element
  std::cout << "The value corresponding to key 'One' is: " << nameNums["One"] << std::endl;

  // Add a few more elements
  nameNums["Two"] = 2;
  nameNums["Three"] = 3;
  nameNums["Four"] = 4;

  // Print the size of the map
  std::cout << "The size of the map is: " << nameNums.size() << std::endl;

  // Only add if the element does not exist
  if(nameNums.count("Four") == 0) {
    std::cout << "Creating new element\n";
    nameNums["Four"] = 4;
  } else {
    std::cout << "The key 'Four' already exist" << std::endl;
  }

  // map pass by refrence
  addKeyValue(&nameNums, "Five", 5);
  std::cout << "Map size is: " << nameNums.size() << std::endl;

  // Loop over the map
  std::map<std::string, int>::iterator it;

  std::cout << "\nMap elements are:\n";
  for(it = nameNums.begin(); it != nameNums.end(); ++it) {
    std::cout << it->first << " => " << it->second << std::endl;
  }
  std::cout << std::endl;
  
  std::cout << "Hello world!" << std::endl;
}
