with import <nixpkgs> {};


stdenv.mkDerivation {

  name = "hello-cmake";
  src = ./.;

  
  nativeBuildInputs = [ cmake ];
  buildInputs = [ boost ];
}
