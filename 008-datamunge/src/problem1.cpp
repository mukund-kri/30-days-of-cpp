#include <map>
#include <string>
#include <fstream>
#include <iostream>

#include <csv/document.hpp>


void problem1(Csv::Document csv_doc) {

  long authorized_cap;
  std::string label;
  
  // Declare and initialaze the counts
  std::vector<std::string> labels { "<1L",
				    "1L to 10L",
				    "10L to 1Cr",
				    "1Cr to 10Cr",
				    "10Cr to 100Cr",
				    ">100Cr" };
  std::map<std::string, int> authorized_cap_acc;
  for(auto const& label : labels) {
    authorized_cap_acc[label] = 0;
  };

  // Loop through rows of csv and compute aggrigates
  for(const auto& row : csv_doc) {

    try {
      authorized_cap = std::stol(row.get("AUTHORIZED_CAP"));

      if (authorized_cap < 100'000l) {
        authorized_cap_acc["<1L"]++;
      } else if (authorized_cap < 1'000'000l) {
        authorized_cap_acc["1L to 10L"]++;
      } else if (authorized_cap < 10'000'000l) {
	authorized_cap_acc["10L to 1Cr"]++;
      } else if (authorized_cap < 100'000'000l) {
        authorized_cap_acc["1Cr to 10Cr"]++; 
      } else if (authorized_cap < 1'000'000'000l) {
	authorized_cap_acc["10Cr to 100Cr"]++;
      } else {
        authorized_cap_acc[">100Cr"]++;
      }
    } catch(...) {
      break;
    }
  }

  // Write out to data file in proper order
  std::ofstream problem1_out ("../plots/problem1.dat");
  for(int i=0; i<labels.size(); i++) {
    label = labels[i];  
    problem1_out << i << "\t"
		 << "\"" << label << "\"" << "\t"
		 << authorized_cap_acc[label]
		 << std::endl;
  }
  problem1_out.close();

}
