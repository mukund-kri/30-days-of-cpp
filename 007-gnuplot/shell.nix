with import <nixpkgs> {};


stdenv.mkDerivation {

  name = "Gnuplot";
  src = ./.;

  nativeBuildInputs = [ cmake ];
  buildInputs = [ boost gnuplot ];

  shellHook = ''
  alias crun='cmake --build . && ./Gnuplot'
  '';
}
