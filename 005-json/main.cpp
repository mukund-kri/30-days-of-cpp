#include <iostream>
#include <fstream>

#include "json/json.h"

int main() {
  Json::Value root;
  Json::StreamWriterBuilder builder;
  const std::unique_ptr<Json::StreamWriter> writer(builder.newStreamWriter());

  std::ofstream outfile;
  outfile.open("test.json");

  root["Name"] = "Mukund";
  root["Age"] = 40;
  writer->write(root, &outfile);

  return EXIT_SUCCESS;
}
