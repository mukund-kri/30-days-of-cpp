# Project name and properties
cmake_minimum_required(VERSION 3.10)
project(Datamunge VERSION 0.0)

# Set the c++ standard to 20
set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED True)

# Add libraries here ...
find_package(Boost COMPONENTS filesystem system iostreams REQUIRED)

set(CSVPP_LIB externals/csvpp)
add_subdirectory(${CSVPP_LIB})
list(APPEND EXTRA_LIBS ${CSVPP_LIB})
list(APPEND EXTRA_LIBS_INCLUDE ${CSVPP_LIB}/src)


# The main executable
file(GLOB SRCS
  src/*.cpp
  src/*.hpp
  )
add_executable(${PROJECT_NAME} ${SRCS})

target_include_directories(${PROJECT_NAME} PUBLIC
  ${EXTRA_LIBS_INCLUDE}
  )

target_link_libraries(${PROJECT_NAME} PUBLIC
  csvpp
  ${Boost_FILESYSTEM_LIBRARY}
  ${Boost_SYSTEM_LIBRARY}
)


