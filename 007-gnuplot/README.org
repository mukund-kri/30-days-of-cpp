* Gnuplot + C++

** Primary Aim

   To discover the use of gnuplot from C++. This is in preparation for
   the data munging project where a whole bunch of plots are required.

** Secondary Aim

   The gist has a whole new directory structure for CMake projects. I
   will try them out in this project and see if it improves my
   workflow. 

   Main ideas ...
   
   - All sources go in the ~srcs~ folder
   - The ~build~ folder in inside the project
   - All the libraries go in the ~externals~ folder
