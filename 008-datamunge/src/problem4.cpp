#include <map>
#include <string>
#include <fstream>
#include <iostream>

#include <csv/document.hpp>
#include <boost/algorithm/string.hpp>


const std::vector<std::string> topPBA =
  {
   "Business Services",
   "Real Estate and Renting",
   "Trading",
   "Construction",
   "Community, personal & Social Services"
  };

const int startYear = 2010;
const int endYear = 2018;


void problem4(Csv::Document csv_doc) {

  // The type defintions are a pain with full namspacing
  using namespace std;

  string date;
  string year;
  vector<string> dateElements;
  
  // our accumulator is a Map of Maps
  map<string, map<string, int>> acc;

  for(const auto& row : csv_doc) {

    // Extract and filte by year of registration
    date = row.get("DATE_OF_REGISTRATION");
      
    boost::split(dateElements, date, boost::is_any_of("-"));
    if (dateElements.size() != 3) continue;

    year = dateElements[2];
    int yearInt = stoi(year);
    
    // Filter by years to be included in the calculation
    if (yearInt < startYear || yearInt > endYear) continue;

    string pba = row.get("PRINCIPAL_BUSINESS_ACTIVITY_AS_PER_CIN");

    acc[year][pba]++;
  }


  ofstream problem4_out ("../plots/problem4.dat");
  
  problem4_out << "#\t";
  for(const auto& business : topPBA) {
    problem4_out << "\"" << business << "\"\t";
  }
  problem4_out << "\n";

  for(const auto& pair : acc) {
    problem4_out << pair.first << "\t";
    for(const auto& business : topPBA) {
      auto mp = pair.second;
      problem4_out << mp[business] << "\t";
    }
    problem4_out << endl;
  }
  
}
