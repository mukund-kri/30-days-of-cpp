with import <nixpkgs> {};


stdenv.mkDerivation {

  name = "Nlohmann_json";
  src = ./.;

  nativeBuildInputs = [ cmake ];
}
