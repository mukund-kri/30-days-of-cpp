#include <map>
#include <set>
#include <string>
#include <fstream>
#include <iostream>

#include <csv/document.hpp>
#include <boost/algorithm/string.hpp>



void problem3(Csv::Document csv_doc) {

  std::string date;
  std::string year;
  std::map<std::string, int> pba_hist;

  // Accumulate by year of registration
  for(const auto& row : csv_doc) {
    date = row.get("DATE_OF_REGISTRATION");
      
    std::vector<std::string> date_elements;

    // If possible extract year out of date. Else ignore row
    boost::split(date_elements, date, boost::is_any_of("-"));
    if (date_elements.size() != 3) continue;
    year = date_elements[2];

    // Only concider records of year 2015
    if (year != "2015") continue;

    std::string pba = row.get("PRINCIPAL_BUSINESS_ACTIVITY_AS_PER_CIN");
    pba_hist[pba]++;
  }

  // Declaring the type of Predicate that accepts 2 pairs and return a bool
  typedef std::function<bool(std::pair<std::string, int>, std::pair<std::string, int>)> Comparator;
 
  // Defining a lambda function to compare two pairs. It will compare two pairs using second field
  Comparator compFunctor = [](std::pair<std::string, int> elem1 ,std::pair<std::string, int> elem2) {
    return elem1.second > elem2.second;
  };

  std::set<std::pair<std::string, int>, Comparator> resultSet (
    pba_hist.begin(), pba_hist.end(), compFunctor);

  // Sort and write out the top 10 to file
  std::ofstream problem3_out ("../plots/problem3.dat");
  int i = 0;
  for(const auto& ele : resultSet) {
    problem3_out << i << "\t"
		 << "\"" << ele.first << "\"\t"
	      << ele.second << std::endl;
    i++;
    if (i >= 10) break;
  }
  problem3_out.close();
  
}
