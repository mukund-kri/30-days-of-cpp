with import <nixpkgs> {};


stdenv.mkDerivation {

  name = "Csv";
  src = ./.;

  nativeBuildInputs = [ cmake ];
  buildInputs = [ boost ];
}
