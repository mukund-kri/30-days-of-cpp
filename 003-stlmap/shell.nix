with import <nixpkgs> {};


stdenv.mkDerivation {

  name = "Stlmap";
  src = ./.;

  nativeBuildInputs = [ cmake ];
}
