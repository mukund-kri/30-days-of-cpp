with import <nixpkgs> {};


stdenv.mkDerivation {

  name = "Json";
  src = ./.;

  nativeBuildInputs = [ cmake ];
}
