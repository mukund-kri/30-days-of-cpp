# Data project with c++ :: West Bengal Company master

## Aim

From the raw CSV data for West Bengal Company data for data.gov.in
plot charts that slice and dice data.

## Tech

* The data slicing / dicing / aggregation is done with C++. The C++
  program takes the raw CSV as input, does the computation and
  generates a plot file compatable with gnuplot.


## Building and Running

1. Clone this repo with --recursive flag. This has libs which are
   submodules. 
2. In the project root create a directory called `build`
3. cd into `build`
4. Initialize the cmake build system with `cmake ../`
5. Build the executable with `cmake --build .`
6. Run the executable `./Datamunge`
7. If every thing went right then gnuplot files should be generated in
   `plots` director of the project root.
8. cd to plots folder and view the plots using gnuplot


## Plotting

1. Make sure gnuplot is installed
2. Start up gnuplot shell `> gnuplot`
3. To plot say the first problem type `load 'problem1.plt'`


  
