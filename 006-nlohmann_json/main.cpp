#include <iostream>
#include <fstream>
#include <iomanip>

#include "nlohmann/json.hpp"


using json = nlohmann::json;

int main() {

  json j2 = {
    {"pi", 3.141},
    {"happy", true},
    {"name", "Niels"},
    {"nothing", nullptr},
    {"answer", {
      {"everything", 42}
    }},
    {"list", {1, 0, 2}},
    {"object", {
      {"currency", "USD"},
      {"value", 42.99}
    }}
  };

  std::ofstream o("pretty.json");
  o << std::setw(4) << j2 << std::endl;
}
