#include <iostream>

#include <csv/document.hpp>


int main() {

  Csv::Document csv_doc;

  csv_doc.set_separator(',');
  csv_doc.load_from_file("mca_westbengal_min.csv");
  

  for(const auto& row : csv_doc) {
    std::cout << row.get("AUTHORIZED_CAP") << std::endl;
  }
}
