cmake_minimum_required(VERSION 3.10)

project(Csv VERSION 0.0)

# Set the c++ standard to 20
set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED True)

find_package(Boost COMPONENTS filesystem system REQUIRED)

# add the csvpp lib subfolder
add_subdirectory(csvpp)
list(APPEND EXTRA_LIBS csvpp)
list(APPEND EXTRA_LIBS_INCLUDE csvpp/src)

# The main executable
add_executable(Main main.cpp)

target_include_directories(Main PUBLIC ${EXTRA_LIBS_INCLUDE})

target_link_libraries(Main PUBLIC
  ${EXTRA_LIBS}
  ${Boost_FILESYSTEM_LIBRARY}
  ${Boost_SYSTEM_LIBRARY}
)
