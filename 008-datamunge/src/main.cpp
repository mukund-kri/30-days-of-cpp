#include <iostream>

#include <csv/document.hpp>

#include "problem1.hpp"
#include "problem2.hpp"
#include "problem3.hpp"
#include "problem4.hpp"


int main() {

  // Load data from CSV file
  Csv::Document csv_doc;
  csv_doc.set_separator(',');
  csv_doc.load_from_file("../data/mca_westbengal.csv");

  std::cout << "Running Problem 1 ..." << std::endl;
  // problem1(csv_doc);

  std::cout << "Running Problem 2 ..." << std::endl;
  // problem2(csv_doc);

  std::cout << "Running Problem 3 ..." << std::endl;
  // problem3(csv_doc);

  std::cout << "Running Problem 4 ..." << std::endl;
  problem4(csv_doc);

  
  return 0;
}
