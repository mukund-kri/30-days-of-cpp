#include <csv/document.hpp>

#include "problem1.hpp"


int main() {

  // Load data from CSV file
  Csv::Document csv_doc;
  csv_doc.set_separator(',');
  csv_doc.load_from_file("../data/mca_westbengal.csv");

  problem1(csv_doc);
  
  return 0;
}
